# openid-connect-4-credential-issuance
Specification to allow holders to request issuance of credentials and issuers to issue verifiable credentials.

### Build the HTML ###

```docker run -v `pwd`:/data danielfett/markdown2rfc openid-4-verifiable-credential-issuance-1_0.md```
